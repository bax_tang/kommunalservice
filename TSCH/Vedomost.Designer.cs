﻿namespace TSCH
{
    partial class Vedomost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label период_оплатыLabel;
            System.Windows.Forms.Label датаLabel;
            System.Windows.Forms.Label тариф_на_услуги_ТОLabel;
            System.Windows.Forms.Label сумма_РРLabel;
            System.Windows.Forms.Label сумма_ДОLabel;
            System.Windows.Forms.Label итогLabel;
            System.Windows.Forms.Label бухгалтерLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vedomost));
            this.kommYslygiDataSet = new TSCH.KommYslygiDataSet();
            this.ведомостьОплатыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ведомостьОплатыTableAdapter = new TSCH.KommYslygiDataSetTableAdapters.ВедомостьОплатыTableAdapter();
            this.tableAdapterManager = new TSCH.KommYslygiDataSetTableAdapters.TableAdapterManager();
            this.ведомостьОплатыBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ведомостьОплатыBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.период_оплатыTextBox = new System.Windows.Forms.TextBox();
            this.датаDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.тариф_на_услуги_ТОTextBox = new System.Windows.Forms.TextBox();
            this.сумма_РРTextBox = new System.Windows.Forms.TextBox();
            this.сумма_ДОTextBox = new System.Windows.Forms.TextBox();
            this.итогTextBox = new System.Windows.Forms.TextBox();
            this.бухгалтерTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            период_оплатыLabel = new System.Windows.Forms.Label();
            датаLabel = new System.Windows.Forms.Label();
            тариф_на_услуги_ТОLabel = new System.Windows.Forms.Label();
            сумма_РРLabel = new System.Windows.Forms.Label();
            сумма_ДОLabel = new System.Windows.Forms.Label();
            итогLabel = new System.Windows.Forms.Label();
            бухгалтерLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.kommYslygiDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ведомостьОплатыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ведомостьОплатыBindingNavigator)).BeginInit();
            this.ведомостьОплатыBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // период_оплатыLabel
            // 
            период_оплатыLabel.AutoSize = true;
            период_оплатыLabel.Location = new System.Drawing.Point(41, 57);
            период_оплатыLabel.Name = "период_оплатыLabel";
            период_оплатыLabel.Size = new System.Drawing.Size(88, 13);
            период_оплатыLabel.TabIndex = 1;
            период_оплатыLabel.Text = "Период оплаты:";
            // 
            // датаLabel
            // 
            датаLabel.AutoSize = true;
            датаLabel.Location = new System.Drawing.Point(41, 84);
            датаLabel.Name = "датаLabel";
            датаLabel.Size = new System.Drawing.Size(36, 13);
            датаLabel.TabIndex = 3;
            датаLabel.Text = "Дата:";
            // 
            // тариф_на_услуги_ТОLabel
            // 
            тариф_на_услуги_ТОLabel.AutoSize = true;
            тариф_на_услуги_ТОLabel.Location = new System.Drawing.Point(41, 109);
            тариф_на_услуги_ТОLabel.Name = "тариф_на_услуги_ТОLabel";
            тариф_на_услуги_ТОLabel.Size = new System.Drawing.Size(112, 13);
            тариф_на_услуги_ТОLabel.TabIndex = 5;
            тариф_на_услуги_ТОLabel.Text = "Тариф на услуги ТО:";
            // 
            // сумма_РРLabel
            // 
            сумма_РРLabel.AutoSize = true;
            сумма_РРLabel.Location = new System.Drawing.Point(41, 135);
            сумма_РРLabel.Name = "сумма_РРLabel";
            сумма_РРLabel.Size = new System.Drawing.Size(135, 13);
            сумма_РРLabel.TabIndex = 7;
            сумма_РРLabel.Text = "Сумма ремонтных работ:";
            // 
            // сумма_ДОLabel
            // 
            сумма_ДОLabel.AutoSize = true;
            сумма_ДОLabel.Location = new System.Drawing.Point(41, 161);
            сумма_ДОLabel.Name = "сумма_ДОLabel";
            сумма_ДОLabel.Size = new System.Drawing.Size(64, 13);
            сумма_ДОLabel.TabIndex = 9;
            сумма_ДОLabel.Text = "Сумма ДО:";
            // 
            // итогLabel
            // 
            итогLabel.AutoSize = true;
            итогLabel.Location = new System.Drawing.Point(41, 187);
            итогLabel.Name = "итогLabel";
            итогLabel.Size = new System.Drawing.Size(34, 13);
            итогLabel.TabIndex = 11;
            итогLabel.Text = "Итог:";
            // 
            // бухгалтерLabel
            // 
            бухгалтерLabel.AutoSize = true;
            бухгалтерLabel.Location = new System.Drawing.Point(41, 213);
            бухгалтерLabel.Name = "бухгалтерLabel";
            бухгалтерLabel.Size = new System.Drawing.Size(61, 13);
            бухгалтерLabel.TabIndex = 13;
            бухгалтерLabel.Text = "Бухгалтер:";
            // 
            // kommYslygiDataSet
            // 
            this.kommYslygiDataSet.DataSetName = "KommYslygiDataSet";
            this.kommYslygiDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ведомостьОплатыBindingSource
            // 
            this.ведомостьОплатыBindingSource.DataMember = "ВедомостьОплаты";
            this.ведомостьОплатыBindingSource.DataSource = this.kommYslygiDataSet;
            // 
            // ведомостьОплатыTableAdapter
            // 
            this.ведомостьОплатыTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = TSCH.KommYslygiDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.ВедомостьОплатыTableAdapter = this.ведомостьОплатыTableAdapter;
            this.tableAdapterManager.Доп_УслугиTableAdapter = null;
            this.tableAdapterManager.ЖильцыTableAdapter = null;
            this.tableAdapterManager.МатериалыTableAdapter = null;
            this.tableAdapterManager.ОплатаTableAdapter = null;
            this.tableAdapterManager.РаботыTableAdapter = null;
            this.tableAdapterManager.РасходыTableAdapter = null;
            // 
            // ведомостьОплатыBindingNavigator
            // 
            this.ведомостьОплатыBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.ведомостьОплатыBindingNavigator.BindingSource = this.ведомостьОплатыBindingSource;
            this.ведомостьОплатыBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.ведомостьОплатыBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.ведомостьОплатыBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.ведомостьОплатыBindingNavigatorSaveItem});
            this.ведомостьОплатыBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ведомостьОплатыBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.ведомостьОплатыBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.ведомостьОплатыBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.ведомостьОплатыBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.ведомостьОплатыBindingNavigator.Name = "ведомостьОплатыBindingNavigator";
            this.ведомостьОплатыBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.ведомостьОплатыBindingNavigator.Size = new System.Drawing.Size(513, 25);
            this.ведомостьОплатыBindingNavigator.TabIndex = 0;
            this.ведомостьОплатыBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ведомостьОплатыBindingNavigatorSaveItem
            // 
            this.ведомостьОплатыBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ведомостьОплатыBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("ведомостьОплатыBindingNavigatorSaveItem.Image")));
            this.ведомостьОплатыBindingNavigatorSaveItem.Name = "ведомостьОплатыBindingNavigatorSaveItem";
            this.ведомостьОплатыBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.ведомостьОплатыBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.ведомостьОплатыBindingNavigatorSaveItem.Click += new System.EventHandler(this.ведомостьОплатыBindingNavigatorSaveItem_Click);
            // 
            // период_оплатыTextBox
            // 
            this.период_оплатыTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ведомостьОплатыBindingSource, "Период оплаты", true));
            this.период_оплатыTextBox.Location = new System.Drawing.Point(187, 54);
            this.период_оплатыTextBox.Name = "период_оплатыTextBox";
            this.период_оплатыTextBox.Size = new System.Drawing.Size(200, 20);
            this.период_оплатыTextBox.TabIndex = 2;
            // 
            // датаDateTimePicker
            // 
            this.датаDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.ведомостьОплатыBindingSource, "Дата", true));
            this.датаDateTimePicker.Enabled = false;
            this.датаDateTimePicker.Location = new System.Drawing.Point(187, 80);
            this.датаDateTimePicker.Name = "датаDateTimePicker";
            this.датаDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.датаDateTimePicker.TabIndex = 4;
            // 
            // тариф_на_услуги_ТОTextBox
            // 
            this.тариф_на_услуги_ТОTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ведомостьОплатыBindingSource, "Тариф на услуги ТО", true));
            this.тариф_на_услуги_ТОTextBox.Location = new System.Drawing.Point(187, 106);
            this.тариф_на_услуги_ТОTextBox.Name = "тариф_на_услуги_ТОTextBox";
            this.тариф_на_услуги_ТОTextBox.Size = new System.Drawing.Size(200, 20);
            this.тариф_на_услуги_ТОTextBox.TabIndex = 6;
            // 
            // сумма_РРTextBox
            // 
            this.сумма_РРTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ведомостьОплатыBindingSource, "Сумма РР", true));
            this.сумма_РРTextBox.Location = new System.Drawing.Point(187, 132);
            this.сумма_РРTextBox.Name = "сумма_РРTextBox";
            this.сумма_РРTextBox.Size = new System.Drawing.Size(200, 20);
            this.сумма_РРTextBox.TabIndex = 8;
            // 
            // сумма_ДОTextBox
            // 
            this.сумма_ДОTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ведомостьОплатыBindingSource, "Сумма ДО", true));
            this.сумма_ДОTextBox.Location = new System.Drawing.Point(187, 158);
            this.сумма_ДОTextBox.Name = "сумма_ДОTextBox";
            this.сумма_ДОTextBox.Size = new System.Drawing.Size(200, 20);
            this.сумма_ДОTextBox.TabIndex = 10;
            // 
            // итогTextBox
            // 
            this.итогTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ведомостьОплатыBindingSource, "Итог", true));
            this.итогTextBox.Location = new System.Drawing.Point(187, 184);
            this.итогTextBox.Name = "итогTextBox";
            this.итогTextBox.Size = new System.Drawing.Size(200, 20);
            this.итогTextBox.TabIndex = 12;
            // 
            // бухгалтерTextBox
            // 
            this.бухгалтерTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ведомостьОплатыBindingSource, "Бухгалтер", true));
            this.бухгалтерTextBox.Location = new System.Drawing.Point(187, 210);
            this.бухгалтерTextBox.Name = "бухгалтерTextBox";
            this.бухгалтерTextBox.Size = new System.Drawing.Size(200, 20);
            this.бухгалтерTextBox.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(410, 130);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(410, 161);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "Рассчитать";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(350, 263);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(144, 39);
            this.button3.TabIndex = 17;
            this.button3.Text = "Начислить";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // Vedomost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 332);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(период_оплатыLabel);
            this.Controls.Add(this.период_оплатыTextBox);
            this.Controls.Add(датаLabel);
            this.Controls.Add(this.датаDateTimePicker);
            this.Controls.Add(тариф_на_услуги_ТОLabel);
            this.Controls.Add(this.тариф_на_услуги_ТОTextBox);
            this.Controls.Add(сумма_РРLabel);
            this.Controls.Add(this.сумма_РРTextBox);
            this.Controls.Add(сумма_ДОLabel);
            this.Controls.Add(this.сумма_ДОTextBox);
            this.Controls.Add(итогLabel);
            this.Controls.Add(this.итогTextBox);
            this.Controls.Add(бухгалтерLabel);
            this.Controls.Add(this.бухгалтерTextBox);
            this.Controls.Add(this.ведомостьОплатыBindingNavigator);
            this.Name = "Vedomost";
            this.Text = "Ведомость оплаты уcлуг на месяц";
            this.Load += new System.EventHandler(this.Vedomost_Load);
            ((System.ComponentModel.ISupportInitialize)(this.kommYslygiDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ведомостьОплатыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ведомостьОплатыBindingNavigator)).EndInit();
            this.ведомостьОплатыBindingNavigator.ResumeLayout(false);
            this.ведомостьОплатыBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KommYslygiDataSet kommYslygiDataSet;
        private System.Windows.Forms.BindingSource ведомостьОплатыBindingSource;
        private KommYslygiDataSetTableAdapters.ВедомостьОплатыTableAdapter ведомостьОплатыTableAdapter;
        private KommYslygiDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator ведомостьОплатыBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton ведомостьОплатыBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox период_оплатыTextBox;
        private System.Windows.Forms.DateTimePicker датаDateTimePicker;
        private System.Windows.Forms.TextBox тариф_на_услуги_ТОTextBox;
        private System.Windows.Forms.TextBox сумма_РРTextBox;
        private System.Windows.Forms.TextBox сумма_ДОTextBox;
        private System.Windows.Forms.TextBox итогTextBox;
        private System.Windows.Forms.TextBox бухгалтерTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}