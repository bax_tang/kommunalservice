﻿namespace TSCH
{
    partial class Rashody
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.kommYslygiDataSet = new TSCH.KommYslygiDataSet();
            this.расходыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.расходыTableAdapter = new TSCH.KommYslygiDataSetTableAdapters.РасходыTableAdapter();
            this.tableAdapterManager = new TSCH.KommYslygiDataSetTableAdapters.TableAdapterManager();
            this.расходыDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.доп_УслугиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.доп_УслугиTableAdapter = new TSCH.KommYslygiDataSetTableAdapters.Доп_УслугиTableAdapter();
            this.доп_УслугиDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.работыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.работыTableAdapter = new TSCH.KommYslygiDataSetTableAdapters.РаботыTableAdapter();
            this.работыDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.материалыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.материалыTableAdapter = new TSCH.KommYslygiDataSetTableAdapters.МатериалыTableAdapter();
            this.материалыDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kommYslygiDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.расходыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.расходыDataGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.доп_УслугиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.доп_УслугиDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.работыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.работыDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.материалыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.материалыDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.расходыDataGridView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(682, 175);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 175);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(682, 297);
            this.panel2.TabIndex = 1;
            // 
            // kommYslygiDataSet
            // 
            this.kommYslygiDataSet.DataSetName = "KommYslygiDataSet";
            this.kommYslygiDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // расходыBindingSource
            // 
            this.расходыBindingSource.DataMember = "Расходы";
            this.расходыBindingSource.DataSource = this.kommYslygiDataSet;
            // 
            // расходыTableAdapter
            // 
            this.расходыTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = TSCH.KommYslygiDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.ВедомостьОплатыTableAdapter = null;
            this.tableAdapterManager.Доп_УслугиTableAdapter = this.доп_УслугиTableAdapter;
            this.tableAdapterManager.ЖильцыTableAdapter = null;
            this.tableAdapterManager.МатериалыTableAdapter = this.материалыTableAdapter;
            this.tableAdapterManager.ОплатаTableAdapter = null;
            this.tableAdapterManager.РаботыTableAdapter = this.работыTableAdapter;
            this.tableAdapterManager.РасходыTableAdapter = this.расходыTableAdapter;
            // 
            // расходыDataGridView
            // 
            this.расходыDataGridView.AutoGenerateColumns = false;
            this.расходыDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.расходыDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.расходыDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.расходыDataGridView.DataSource = this.расходыBindingSource;
            this.расходыDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.расходыDataGridView.Location = new System.Drawing.Point(0, 0);
            this.расходыDataGridView.Name = "расходыDataGridView";
            this.расходыDataGridView.Size = new System.Drawing.Size(682, 175);
            this.расходыDataGridView.TabIndex = 0;
            this.расходыDataGridView.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.расходыDataGridView_RowLeave);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Период оплаты";
            this.dataGridViewTextBoxColumn1.HeaderText = "Период оплаты";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ID_услугиДО";
            this.dataGridViewTextBoxColumn2.HeaderText = "ID_услугиДО";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ID_работы";
            this.dataGridViewTextBoxColumn3.HeaderText = "ID_работы";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Дата";
            this.dataGridViewTextBoxColumn4.HeaderText = "Дата";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Стоимость";
            this.dataGridViewTextBoxColumn5.HeaderText = "Стоимость";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(682, 297);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.доп_УслугиDataGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(674, 271);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Дополнительные услуги";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.материалыDataGridView);
            this.tabPage2.Controls.Add(this.работыDataGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(674, 271);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ремонтные работы";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // доп_УслугиBindingSource
            // 
            this.доп_УслугиBindingSource.DataMember = "Доп_Услуги";
            this.доп_УслугиBindingSource.DataSource = this.kommYslygiDataSet;
            // 
            // доп_УслугиTableAdapter
            // 
            this.доп_УслугиTableAdapter.ClearBeforeFill = true;
            // 
            // доп_УслугиDataGridView
            // 
            this.доп_УслугиDataGridView.AutoGenerateColumns = false;
            this.доп_УслугиDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.доп_УслугиDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.доп_УслугиDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.доп_УслугиDataGridView.DataSource = this.доп_УслугиBindingSource;
            this.доп_УслугиDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.доп_УслугиDataGridView.Location = new System.Drawing.Point(3, 3);
            this.доп_УслугиDataGridView.Name = "доп_УслугиDataGridView";
            this.доп_УслугиDataGridView.Size = new System.Drawing.Size(668, 265);
            this.доп_УслугиDataGridView.TabIndex = 0;
            this.доп_УслугиDataGridView.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.доп_УслугиDataGridView_RowLeave);
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ID_ДО";
            this.dataGridViewTextBoxColumn6.HeaderText = "ID_ДО";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Виды";
            this.dataGridViewTextBoxColumn7.HeaderText = "Виды";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Стоимоть";
            this.dataGridViewTextBoxColumn8.HeaderText = "Стоимоть";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // работыBindingSource
            // 
            this.работыBindingSource.DataMember = "Работы";
            this.работыBindingSource.DataSource = this.kommYslygiDataSet;
            // 
            // работыTableAdapter
            // 
            this.работыTableAdapter.ClearBeforeFill = true;
            // 
            // работыDataGridView
            // 
            this.работыDataGridView.AutoGenerateColumns = false;
            this.работыDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.работыDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.работыDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            this.работыDataGridView.DataSource = this.работыBindingSource;
            this.работыDataGridView.Dock = System.Windows.Forms.DockStyle.Top;
            this.работыDataGridView.Location = new System.Drawing.Point(3, 3);
            this.работыDataGridView.Name = "работыDataGridView";
            this.работыDataGridView.Size = new System.Drawing.Size(668, 112);
            this.работыDataGridView.TabIndex = 0;
            this.работыDataGridView.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.работыDataGridView_RowLeave);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "ID_работы";
            this.dataGridViewTextBoxColumn9.HeaderText = "ID_работы";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Название";
            this.dataGridViewTextBoxColumn10.HeaderText = "Название";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Стоимость";
            this.dataGridViewTextBoxColumn11.HeaderText = "Стоимость";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // материалыBindingSource
            // 
            this.материалыBindingSource.DataMember = "Материалы";
            this.материалыBindingSource.DataSource = this.kommYslygiDataSet;
            // 
            // материалыTableAdapter
            // 
            this.материалыTableAdapter.ClearBeforeFill = true;
            // 
            // материалыDataGridView
            // 
            this.материалыDataGridView.AutoGenerateColumns = false;
            this.материалыDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.материалыDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.материалыDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16});
            this.материалыDataGridView.DataSource = this.материалыBindingSource;
            this.материалыDataGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.материалыDataGridView.Location = new System.Drawing.Point(3, 121);
            this.материалыDataGridView.Name = "материалыDataGridView";
            this.материалыDataGridView.Size = new System.Drawing.Size(668, 147);
            this.материалыDataGridView.TabIndex = 1;
            this.материалыDataGridView.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.материалыDataGridView_RowLeave);
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "ID_материала";
            this.dataGridViewTextBoxColumn12.HeaderText = "ID_материала";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "ID_работы";
            this.dataGridViewTextBoxColumn13.HeaderText = "ID_работы";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Название";
            this.dataGridViewTextBoxColumn14.HeaderText = "Название";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Количество";
            this.dataGridViewTextBoxColumn15.HeaderText = "Количество";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Цена";
            this.dataGridViewTextBoxColumn16.HeaderText = "Цена";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // Rashody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 472);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Rashody";
            this.Text = "Расходы на ремонтные работы и дополнительные услуги";
            this.Load += new System.EventHandler(this.Rashody_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kommYslygiDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.расходыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.расходыDataGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.доп_УслугиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.доп_УслугиDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.работыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.работыDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.материалыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.материалыDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private KommYslygiDataSet kommYslygiDataSet;
        private System.Windows.Forms.BindingSource расходыBindingSource;
        private KommYslygiDataSetTableAdapters.РасходыTableAdapter расходыTableAdapter;
        private KommYslygiDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView расходыDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private KommYslygiDataSetTableAdapters.Доп_УслугиTableAdapter доп_УслугиTableAdapter;
        private System.Windows.Forms.BindingSource доп_УслугиBindingSource;
        private System.Windows.Forms.DataGridView доп_УслугиDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private KommYslygiDataSetTableAdapters.РаботыTableAdapter работыTableAdapter;
        private System.Windows.Forms.BindingSource работыBindingSource;
        private System.Windows.Forms.DataGridView работыDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private KommYslygiDataSetTableAdapters.МатериалыTableAdapter материалыTableAdapter;
        private System.Windows.Forms.BindingSource материалыBindingSource;
        private System.Windows.Forms.DataGridView материалыDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
    }
}