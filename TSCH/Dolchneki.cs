﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;


namespace TSCH
{
    public partial class Dolchneki : Form
    {
        public Dolchneki()
        {
            InitializeComponent();
        }

        private void Dolchneki_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "KommYslygiDataSet.View_Dolchneki". При необходимости она может быть перемещена или удалена.
            this.View_DolchnekiTableAdapter.Fill(this.KommYslygiDataSet.View_Dolchneki);

            this.reportViewer1.RefreshReport();
        }
    }
}
