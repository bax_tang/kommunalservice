﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TSCH
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void жильцыBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.жильцыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "kommYslygiDataSet.Оплата". При необходимости она может быть перемещена или удалена.
            this.оплатаTableAdapter.Fill(this.kommYslygiDataSet.Оплата);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "kommYslygiDataSet.Жильцы". При необходимости она может быть перемещена или удалена.
            this.жильцыTableAdapter.Fill(this.kommYslygiDataSet.Жильцы);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Rashody fr = new Rashody();
            fr.Show();

        }

        private void оплатаDataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            this.Validate();
            this.оплатаBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            жильцыDataGridView.DataSource = null;
            жильцыDataGridView.DataSource = жильцыBindingSource;
            жильцыBindingSource.Filter = " [ФИО] LIKE'" + textBox1.Text + "%'";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            жильцыDataGridView.DataSource = null;
            жильцыDataGridView.DataSource = жильцыBindingSource;
            жильцыBindingSource.Filter = " [Счет] LIKE'" + textBox2.Text + "%'";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Все")
            {
                жильцыDataGridView.DataSource = null;
                жильцыDataGridView.DataSource = жильцыBindingSource;
            
            }
            else
            {
                жильцыDataGridView.DataSource = null;
                жильцыDataGridView.DataSource = жильцыBindingSource;
                жильцыBindingSource.Filter = string.Format("Convert([Номер дома], System.String) LIKE '%{0}%'", comboBox1.Text);
            }
        }

        private void жильцыDataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            this.Validate();
            this.жильцыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Vedomost fr = new Vedomost();
            fr.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Dolchneki fr = new Dolchneki();
            fr.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Diagram fr = new Diagram();
            fr.Show();
        }




    }
}
