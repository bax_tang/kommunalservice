﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TSCH
{
    public partial class Rashody : Form
    {
        public Rashody()
        {
            InitializeComponent();
        }

        private void расходыBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.расходыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);

        }

        private void Rashody_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "kommYslygiDataSet.Материалы". При необходимости она может быть перемещена или удалена.
            this.материалыTableAdapter.Fill(this.kommYslygiDataSet.Материалы);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "kommYslygiDataSet.Работы". При необходимости она может быть перемещена или удалена.
            this.работыTableAdapter.Fill(this.kommYslygiDataSet.Работы);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "kommYslygiDataSet.Доп_Услуги". При необходимости она может быть перемещена или удалена.
            this.доп_УслугиTableAdapter.Fill(this.kommYslygiDataSet.Доп_Услуги);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "kommYslygiDataSet.Расходы". При необходимости она может быть перемещена или удалена.
            this.расходыTableAdapter.Fill(this.kommYslygiDataSet.Расходы);

        }

        private void расходыDataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            this.Validate();
            this.расходыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);
        }

        private void доп_УслугиDataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            this.Validate();
            this.доп_УслугиBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);
        }

        private void работыDataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            this.Validate();
            this.работыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);
        }

        private void материалыDataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            this.Validate();
            this.материалыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.kommYslygiDataSet);
        }
    }
}
