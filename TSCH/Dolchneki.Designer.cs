﻿namespace TSCH
{
    partial class Dolchneki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.View_DolchnekiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.KommYslygiDataSet = new TSCH.KommYslygiDataSet();
            this.View_DolchnekiTableAdapter = new TSCH.KommYslygiDataSetTableAdapters.View_DolchnekiTableAdapter();
            this.tableAdapterManager = new TSCH.KommYslygiDataSetTableAdapters.TableAdapterManager();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.View_DolchnekiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KommYslygiDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // View_DolchnekiBindingSource
            // 
            this.View_DolchnekiBindingSource.DataMember = "View_Dolchneki";
            this.View_DolchnekiBindingSource.DataSource = this.KommYslygiDataSet;
            // 
            // KommYslygiDataSet
            // 
            this.KommYslygiDataSet.DataSetName = "KommYslygiDataSet";
            this.KommYslygiDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // View_DolchnekiTableAdapter
            // 
            this.View_DolchnekiTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = TSCH.KommYslygiDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.ВедомостьОплатыTableAdapter = null;
            this.tableAdapterManager.Доп_УслугиTableAdapter = null;
            this.tableAdapterManager.ЖильцыTableAdapter = null;
            this.tableAdapterManager.МатериалыTableAdapter = null;
            this.tableAdapterManager.ОплатаTableAdapter = null;
            this.tableAdapterManager.РаботыTableAdapter = null;
            this.tableAdapterManager.РасходыTableAdapter = null;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.View_DolchnekiBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "TSCH.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(726, 367);
            this.reportViewer1.TabIndex = 0;
            // 
            // Dolchneki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 367);
            this.Controls.Add(this.reportViewer1);
            this.Name = "Dolchneki";
            this.Text = "Злостные неплательщики";
            this.Load += new System.EventHandler(this.Dolchneki_Load);
            ((System.ComponentModel.ISupportInitialize)(this.View_DolchnekiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KommYslygiDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource View_DolchnekiBindingSource;
        private KommYslygiDataSet KommYslygiDataSet;
        private KommYslygiDataSetTableAdapters.View_DolchnekiTableAdapter View_DolchnekiTableAdapter;
        private KommYslygiDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}